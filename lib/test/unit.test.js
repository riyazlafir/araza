"use strict";
/*
This will handle all the relevant unit tests.
*/
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var InsertOrder = require("../src/order");
var User = require("../src/getuser");
var Product = require("../src/getproduct");
var OrderDetails = require("../src/getorderDetails");
var insertUser = require("../src/adduser");
var insertProductDetails = require("../src/addproduct");
var dbName = 'ArazaTest';
var MongoClient = require('mongodb').MongoClient;
var MongoMemoryServer = require('mongodb-memory-server').MongoMemoryServer;
describe('Test servcie functions', function () {
    var connection;
    var db;
    var userId;
    var productId;
    var server;
    var name = "Test user";
    var productName = "HP laptop";
    server = new MongoMemoryServer();
    beforeAll(function () { return __awaiter(void 0, void 0, void 0, function () {
        var _a, _b;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    _b = (_a = MongoClient).connect;
                    return [4 /*yield*/, server.getConnectionString()];
                case 1: return [4 /*yield*/, _b.apply(_a, [_c.sent(), {
                            useNewUrlParser: true, useUnifiedTopology: true
                        }])];
                case 2:
                    connection = _c.sent();
                    return [4 /*yield*/, connection.db(dbName)];
                case 3:
                    db = _c.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    afterAll(function () { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, connection.close()];
                case 1:
                    _a.sent();
                    return [4 /*yield*/, db.close()];
                case 2:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    it('Create an user', function () {
        return insertUser.insertUser(db, name).then(function (data) {
            userId = data;
            expect(data).not.toBe('Error');
        });
    });
    it('Create a product', function () {
        return insertProductDetails.insertProduct(db, productName, 1500).then(function (data) {
            productId = data;
            expect(data).not.toBe('Error');
        });
    });
    it('Get an user', function () {
        return User.fetchUserById(db, userId).then(function (data) {
            userId = data.userid;
            expect(data.name).toBe(name);
        });
    });
    it('Get a product', function () {
        return Product.fetchProductById(db, productId).then(function (data) {
            productId = data.productid;
            expect(data.name).toBe(productName);
        });
    });
    it('Create an order', function () {
        return InsertOrder.insertOrder(db, productId, userId).then(function (data) {
            expect(data).toBe('Success');
        });
    });
    it('Get all order list by user', function () {
        return OrderDetails.fetchOrderDetailsByUser(db, userId).then(function (data) {
            expect(typeof data).toBe('object');
            expect(data).not.toBe('Error');
        });
    });
    it('Get all order list by product id', function () {
        return OrderDetails.fetchOrderDetailsByProduct(db, productId).then(function (data) {
            expect(typeof data).toBe('object');
            expect(data).not.toBe('Error');
        });
    });
});
