"use strict";
/*
This will work as a entry point to the application.
We can use this function to call other service functions for demo purpose.
*/
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var DbClient = require("./connection/ConnectServer");
var Order = require("./order");
var User = require("./getuser");
var Product = require("./getproduct");
var OrderDetails = require("./getorderDetails");
var insertUserDetails = require("./adduser");
var insertProductDetails = require("./addproduct");
var App = /** @class */ (function () {
    function App() {
    }
    App.prototype.start = function () {
        return __awaiter(this, void 0, void 0, function () {
            var dbName, client, db, userId, productId, inserProduct, insertUser, userById, productById, orderDetails, orderDetailsByProduct, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("Start application...!");
                        dbName = 'ArazaTest';
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 10, , 11]);
                        return [4 /*yield*/, DbClient.connect()];
                    case 2:
                        client = _a.sent();
                        db = client.db(dbName);
                        userId = null;
                        productId = null;
                        return [4 /*yield*/, insertProductDetails.insertProduct(db, "Lenovo laptop", 1500)];
                    case 3:
                        inserProduct = _a.sent();
                        productId = inserProduct;
                        return [4 /*yield*/, insertUserDetails.insertUser(db, "Riyaz")];
                    case 4:
                        insertUser = _a.sent();
                        userId = insertUser;
                        return [4 /*yield*/, User.fetchUserById(db, userId)];
                    case 5:
                        userById = _a.sent();
                        console.log(" Fetch a user details : \n", userById);
                        return [4 /*yield*/, Product.fetchProductById(db, productId)];
                    case 6:
                        productById = _a.sent();
                        console.log("\n Fetch a product details :\n ", productById);
                        // Create an order
                        return [4 /*yield*/, Order.insertOrder(db, productById.productid, userById.userid)
                            // Fetch Order details by user
                        ];
                    case 7:
                        // Create an order
                        _a.sent();
                        return [4 /*yield*/, OrderDetails.fetchOrderDetailsByUser(db, userById.userid)];
                    case 8:
                        orderDetails = _a.sent();
                        console.log("\n Orders list by user :\n", orderDetails);
                        return [4 /*yield*/, OrderDetails.fetchOrderDetailsByProduct(db, productById.productid)];
                    case 9:
                        orderDetailsByProduct = _a.sent();
                        console.log("\n Orders list by product id: \n", orderDetailsByProduct);
                        return [3 /*break*/, 11];
                    case 10:
                        error_1 = _a.sent();
                        console.log("Unable to connect to db", error_1);
                        return [3 /*break*/, 11];
                    case 11: return [2 /*return*/];
                }
            });
        });
    };
    return App;
}());
var app = new App();
app.start();
