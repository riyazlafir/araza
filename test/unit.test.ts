/*
This will handle all the relevant unit tests.
*/


import InsertOrder = require("../src/order");
import User = require("../src/getuser");
import Product = require("../src/getproduct");
import OrderDetails = require("../src/getorderDetails");
import insertUser = require("../src/adduser");
import insertProductDetails = require("../src/addproduct");

const dbName = 'ArazaTest';
const { MongoClient } = require('mongodb');
const { MongoMemoryServer } = require('mongodb-memory-server');


describe('Test servcie functions', () => {
    let connection: any;
    let db: any;
    let userId: any;    
    let productId: any;
    let server: any;
    let name: string = "Test user";
    let productName: string = "HP laptop";
    server = new MongoMemoryServer();
    beforeAll(async () => {
        connection = await MongoClient.connect(await server.getConnectionString(), {
            useNewUrlParser: true,useUnifiedTopology: true
        });
        db = await connection.db(dbName);
    });

    afterAll(async () => {
        await connection.close();
        await db.close();
    });

    it('Create an user', () => {
        return insertUser.insertUser(db, name).then(data => {           
            userId = data;    
            expect(data).not.toBe('Error');
        });
    });


    it('Create a product', () => {
        return insertProductDetails.insertProduct(db,  productName, 1500).then(data => {           
            productId = data;
            expect(data).not.toBe('Error');
        });
    });

    it('Get an user', () => {
        return User.fetchUserById(db, userId).then(data => { 
            
            userId = data.userid;          
             expect(data.name).toBe(name);
        });
    });


    it('Get a product', () => {
        return Product.fetchProductById(db, productId).then(data => { 
            productId = data.productid;          
             expect(data.name).toBe(productName);
        });
    });


    it('Create an order', () => {
        return InsertOrder.insertOrder(db, productId, userId).then(data => {
            expect(data).toBe('Success');
        });
    });

    it('Get all order list by user', () => {
        return OrderDetails.fetchOrderDetailsByUser(db, userId).then(data => {
 
            expect(typeof data).toBe('object')
            expect(data).not.toBe('Error');
        });
    });


    it('Get all order list by product id', () => {
        return OrderDetails.fetchOrderDetailsByProduct(db, productId).then(data => { 
            expect(typeof data).toBe('object')
            expect(data).not.toBe('Error');
        });
    });
});