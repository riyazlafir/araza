# Araza
Please follow the below steps:

1) In the root folder run command `tsc`. 
(This command is mandatory.This will compile type script file into javascript file under lib folder. Make sure typescript installed globally `npm install -g typescript ts-node`. )

2) For unit testing run the following command in the root folder `npm test`.
3) For Run the application execute the following command in the root folder `npm start`. 

-----------------------------------------------------------

Expected(similar) output for `npm test` command.

 PASS  lib/test/unit.test.js (5.278s)
  Test servcie functions
    √ Create an user (18ms)
    √ Create a product (7ms)
    √ Get an user (7ms)
    √ Get a product (3ms)
    √ Create an order (5ms)
    √ Get all order list by user (5ms)
    √ Get all order list by product id (3ms)

Test Suites: 1 passed, 1 total
Tests:       7 passed, 7 total
Snapshots:   0 total
Time:        6.53s, estimated 9s
Ran all test suites.

----------------------------------------------------

Expected(similar) output `npm start` command.

Fetch a user details :
 { _id: 5e39fb9731d67921f8867084,
  name: 'Riyaz',
  userid: '2d96b405c36b3b4b' }

 Fetch a product details :
  { _id: 5e39fb9731d67921f8867083,
  name: 'Lenovo laptop',
  productid: '007b115051988388',
  price: 1500 }

 Orders list by user :
 [ { _id: 5e39fb9731d67921f8867085,
    userid: '2d96b405c36b3b4b',
    productid: '007b115051988388',
    orderid: 'f4da2cc34d5b2946',
    date: 2020-02-04T23:17:43.826Z,
    status: 'pending' } ]

 Orders list by product id:
 [ { _id: 5e39fb9731d67921f8867085,
    userid: '2d96b405c36b3b4b',
    productid: '007b115051988388',
    orderid: 'f4da2cc34d5b2946',
    date: 2020-02-04T23:17:43.826Z,
    status: 'pending' } ]

-------------------------------------------

I have addressed all the questions mentioned in the email.

"Create service functions to:  

- Create an order

- Fetch a user

- Fetch a user and all their orders

- Fetch a product

- Fetch a product and all its orders. "
