/*
This will handle database connection process. 
All the service functions will utilize this function to make connection with mongodb server.
*/

import { MongoClient, Db } from "mongodb";
const { MongoMemoryServer } = require('mongodb-memory-server');
class ConnectServer {

    db: any = null;
    server: any = null;

    constructor() {
        this.server = new MongoMemoryServer();
        this.db = null;
    }
    public async connect() {


        try {

            // Connect to mongodb-memory-server 
            const url = await this.server.getConnectionString();
            this.db = await MongoClient.connect(url, { useNewUrlParser: true ,useUnifiedTopology: true  });

            // Connect to mLab server
            //this.db = await MongoClient.connect("mongodb+srv://dbUser:1qaz@wsX@cluster0-ln0v3.gcp.mongodb.net/test?retryWrites=true&w=majority");
            console.log("Successfully connected!");
            return this.db;
        } catch (error) {

        }

    }
}

export = new ConnectServer();