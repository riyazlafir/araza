 /*
This service will retrieve product details based on product id or all product list. 
API handlers can call this service to retrieve the products.
*/
class GetProduct {
   
    db: any= null;

constructor() { 
   
   this.db = null;
}

// This method will retrieve product details based on product id.
    public async fetchProductById(db : any ,  pId: string) {
      
             
            let productId = pId;
            let query = { productid: productId };
            let docs = await db.collection("product").findOne(query);
            return docs;                 
             
    }

 
// This method will retrieve all the product details list.
public async fetchAllProducts(db : any ) {
      
             
  
    let docs = await db.collection("product").find().toArray();
    return docs;
       
}

}

 
export = new GetProduct();