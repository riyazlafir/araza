/*
This service will handle insert a new product into the db. 
API handlers can call this service to insert a new product.
*/

import { Double } from "bson";

class InsertProduct {

    db: any = null;

    constructor() {

        this.db = null;
    }

    public async insertProduct(db: any, pName: string, productPrice: Double) {

        try {
            let productName = pName;
            let price = productPrice;
            let id;

            await generateId().then(response => {
                id = response;

            }).catch(err => console.log("Error :" + err));

            await db.collection("product").insertOne({
                name: productName,
                productid: id,
                price: price
            });
            return id;
        } catch (error) {
            return "Error";
        }



    }
}

var generateId = () => new Promise(async function (resolve, reject) {
    require('crypto').randomBytes(8, function (ex: any, buf: any) {
        var token = buf.toString('hex');

        resolve(token);
    })
});
export = new InsertProduct();