/*
This service will retrieve order details based on user or product. 
API handlers can call this service to retrieve the orders.
*/
class GetOrderDetails {

    db: any = null;

    constructor() {

        this.db = null;
    }

    // This method will retrieve order details based on user.
    public async fetchOrderDetailsByUser(db: any, uId: string) {
        try {
            let userId = uId;
            let query = { userid: userId };
            let docs = await db.collection("order").find(query).toArray();
            return docs;
        } catch (error) {
            return "Error";
        }

    }

    // This method will retrieve order details based on product.
    public async fetchOrderDetailsByProduct(db: any, pId: string) {

        try {
            let productId = pId;
            let query = { productid: productId };
            let docs = await db.collection("order").find(query).toArray();
            return docs;
        } catch (error) {
            return "Error"
        }

    }

}


export = new GetOrderDetails();