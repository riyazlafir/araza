/*
This will work as a entry point to the application. 
We can use this function to call other service functions for demo purpose.
*/

import DbClient = require("./connection/ConnectServer");
import Order = require("./order");
import User = require("./getuser");
import Product = require("./getproduct");
import OrderDetails = require("./getorderDetails");
import insertUserDetails = require("./adduser");
import insertProductDetails = require("./addproduct");
 
class App {
    public async start() {
        console.log("Start application...!");
        const dbName = 'ArazaTest';

        try {
            let client = await DbClient.connect();
            let db = client.db(dbName);

            let userId: any = null;
            let productId: any = null;


            // Create a product
            let inserProduct = await insertProductDetails.insertProduct(db, "Lenovo laptop", 1500);
            productId = inserProduct;

            // Create a user
            let insertUser = await insertUserDetails.insertUser(db, "Riyaz");
            userId = insertUser;


            // Fetch user by Id
            let userById = await User.fetchUserById(db, userId);
            console.log(" Fetch a user details : \n", userById);

            // Fetch product by Id
            let productById = await Product.fetchProductById(db, productId);
            console.log("\n Fetch a product details :\n ", productById);


            // Create an order
            await Order.insertOrder(db, productById.productid, userById.userid)


            // Fetch Order details by user
            let orderDetails = await OrderDetails.fetchOrderDetailsByUser(db, userById.userid);
            console.log("\n Orders list by user :\n", orderDetails);

            // Fetch Order details by product
            let orderDetailsByProduct = await OrderDetails.fetchOrderDetailsByProduct(db, productById.productid);
            console.log("\n Orders list by product id: \n", orderDetailsByProduct);



            // Fetch all users
            //let userList = await User.fetchAllUser(db);
            //console.log("\n User list :", userList);

            // Fetch all products
            // let productList = await Product.fetchAllProducts(db);
            // console.log("\n Product list :", productList);



        } catch (error) {
            console.log("Unable to connect to db", error);

        }
    }
}

const app = new App();
app.start();