/*
This service will insert new order into db. 
API handlers can call this service to inset new orders.
*/
class Order {

    db: any = null;

    constructor() {

        this.db = null;
    }

    public async insertOrder(db: any, pId: string, uId: string) {

         
        let productId = pId;
        let userId = uId;
        let id = null;
        let status = "pending";
        let orderDate = new Date();
        await generateId().then(response => {
            id = response;

        }).catch(err => console.log("Error :" + err));

        await db.collection("order").insertOne({
            userid: userId,
            productid: productId,
            orderid: id,
            date: orderDate,
            status: status
        });
  return "Success";
    }
}

var generateId = () => new Promise(async function (resolve, reject) {
    require('crypto').randomBytes(8, function (ex: any, buf: any) {
        var token = buf.toString('hex');

        resolve(token);
    })
});
export = new Order();