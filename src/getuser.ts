/*
This service will retrieve user details based on user id or all user list. 
API handlers can call this service to retrieve the user.
*/
class GetUser {

    db: any = null;

    constructor() {

        this.db = null;
    }
    // This method will retrieve user details based on user id.
    public async fetchUserById(db: any, uId: string) {


        let userId = uId;
        let query = { userid: userId };
        let docs = await db.collection("user").findOne(query);
        return docs;

    }


    // This method will retrieve all user details list.
    public async fetchAllUser(db: any) {

        let docs = await db.collection("user").find().toArray();
        return docs;

    }

}


export = new GetUser();