/*
This will handle new user insert task. 
API handlers can call this service function to add the new user.
*/

class InsertUser {

    db: any = null;

    constructor() {

        this.db = null;
    }

    public async insertUser(db: any, username: string) {

        try {
            let name = username;
            let id = null;

            await generateId().then(response => {
                id = response;

            }).catch(err => console.log("Error :" + err));

            await db.collection("user").insertOne({
                name: name, userid: id
            });

             
            return id;
        } catch (error) {
            return "Error";
        }


    }
}

var generateId = () => new Promise(async function (resolve, reject) {
    require('crypto').randomBytes(8, function (ex: any, buf: any) {
        var token = buf.toString('hex');

        resolve(token);
    })
});
export = new InsertUser();